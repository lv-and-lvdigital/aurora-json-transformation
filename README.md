# Library to handle Aurora Publications

Library to handle an AWS subscription from a Woodwing Aurora publication, download and transform data from JSON to a customizable PHP array.

Woodwing Aurora is a Multi-Channel-Publishing plattform to aim the goal "write once, publish anywhere". Aurora publishes its content in different formats like HTML, PSV and JSON. This library concentrate on custom transformation of the proprietary Aurora JSON format to an PHP array.

You find additional Information about the JSON-Format:
https://helpcenter.woodwing.com/hc/en-us/articles/360000130186-The-JSON-Digital-article-format-specification

# Requirements

- PHP >= 7.2.5

# Installation

    composer require lv/aurorajson:dev-master

Configure a Publication Channel in Woodwing Aurora.

Setup a website to get publication data from Woodwing Aurora.
You find in public/index.php the stuff to handle AWSSubscription and trigger the JSON transformation process.
You find rules for the transformation process in config/config.json. See the usage section.

# Usage

In public/index.php is the part to handle AWSSubscription and trigger the PHP Array Transformation:

    /* Download Manager is responsible to get article data from AWS-Cloud */
    $aDownload = new DownloadManager($configData);
    $aDownload->handleAwsSnsMessage();
    if ($aDownload->isDownloadSuccess()) {
        $artDao = new ArticleManager($configData);
        $articleData = $artDao->parseJsonData($aDownload->getArticleJsonData());
    }

# Credits
