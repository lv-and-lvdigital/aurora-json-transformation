#!/bin/bash -ex

# PACKAGIST_USERNAME        (Required) Username on Packagist
#                               Example: "devpartisan"
# PACKAGIST_API_TOKEN       (Required) API token on Packagist
#                               See https://packagist.org/profile/ for API token
# PACKAGIST_PACKAGE_URL     (Required) Package URL on Packagist
#                               Example: "https://packagist.org/packages/monolog/monolog"
PACKAGIST_USERNAME=$PACKAGIST_USERNAME
PACKAGIST_API_TOKEN=$PACKAGIST_API_TOKEN
URL="https://packagist.org/api/update-package"\
"?username=${PACKAGIST_USERNAME}"\
"&apiToken=${PACKAGIST_API_TOKEN}"
curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"repository":{"url":"https://packagist.org/packages/lv/aurorajson"}}' \
    ${URL}
