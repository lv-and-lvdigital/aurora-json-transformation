<?php

use lv\aurorajson\manager\ArticleManager;
use lv\aurorajson\manager\DownloadManager;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Noodlehaus\Config;


require_once __DIR__ . '/../vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors','On');


/* load config of each website from config.json */
$conf = Config::load('../config/config.json');
$configData = $conf->all();


/** @var Monolog\Logger $log */
$log = new Logger('wac');
$log->pushHandler(new StreamHandler($configData['log'], Logger::INFO));

/* Download Manager is responsible to get article data from AWS-Cloud */
$aDownload = new DownloadManager($configData);
/* Handle AWS Request */

$artDao = new ArticleManager($configData);
//$data = file_get_contents("../json-digital-article-format-specification/937/article1.json");
$data = file_get_contents("../json-digital-article-format-specification/8710/article1.json");
//$data = file_get_contents("../json-digital-article-format-specification/869/article1.json");
$ret = $artDao->parseJsonData($data);
print_r( $ret);

/*
$aDownload->handleAwsSnsMessage();
if ($aDownload->isDownloadSuccess()) {
    $artDao = new \ProjectRoot\src\manager\ArticleManager($configData);
    $articleData = $artDao->parseJsonData($aDownload->getArticleJsonData());

    $log->info("data2: " . var_export($articleData, true));
}
*/


?>