<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 17.01.2019
 * Time: 10:29
 */

namespace lv\aurorajson\manager;


use JsonPath\JsonObject;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use ReflectionException;
use ReflectionMethod;

/**
 * Class ArticleManager
 *
 *
 * @package lv\aurorajson\manager
 *
 */
class ArticleManager
{
    /** @var array */
    private $configData;
    /** @var Logger */
    private $log;

    /*
     * @param array
     * ADownloadManager constructor.
     */
    public function __construct($configData)
    {
        $this->configData = $configData;

        /** @var Logger $log */
        $this->log = new Logger('wac');
        $this->log->pushHandler(new StreamHandler($configData['log'], Logger::INFO));
    }

    public function parseJsonData($articleJsonData)
    {
        $jB = new JsonObject($articleJsonData, true);
        $jsonTransArray = $this->configData['jsonTrans'];
        $all = [];
        foreach ($jsonTransArray as $jst) {
            $value = $jB->{$jst["value"]};
            // Remove empty components
            if( !empty($value)) {
                foreach( $value as $k => $v) {
                    if(empty($v['content']) && $v['identifier'] != 'slideshow' && $v['identifier'] != 'hint' && $v['identifier'] != 'ordered-list' && $v['identifier'] != 'unordered-list' && $v['identifier'] != 'container' && $v['identifier'] != 'paywall-separator' ) {
                        unset( $value[$k]);
                    }
                }
                if( empty( $value)) {
                    continue;
                }
            }
            /**
             * Transformation einer Komponente
             */
            if (class_exists($jst["transformer"])) {
                try {
                    $transformer = new $jst["transformer"]();
                    $reflectionMethod = new ReflectionMethod($jst["transformer"],"transform");
                    $value = $reflectionMethod->invokeArgs($transformer,array($value)); // throws exception if parameters are wrong
                } catch (ReflectionException $e) {
                    $this->log->error("error executing transformation: class: " . $jst["transformer"] . ' Exception: ' . $e->getMessage());
                }

                }
//            $message = $jst["key"] . ' : ' . print_r($value, true);
//            echo '<p>' . $message;
//            echo '<br>-------------------------------------------------------------------';
//            $this->log->info($message);
            $all = array_merge($all, $value);
        }
//        print_r( $all);
        $text = [];
        $media = ['image' =>[], 'video' => [], 'google-maps' => [], 'slideshow' => []];
        $elements = $jB->{'$.data.content'};
//        print_r( $data);
        foreach( $elements as $element ) {
            $id = $element['id'];
            $this->sortElements( $id, $all, $text, $media, true);
        }
        return [
            'text' => $text,
//            'media' => $media
        ];
    }

    private function sortElements( $id, $all, &$text, &$media, $first) {
        $this->log->info( "sortElements $id");
        if( !isset( $all[$id])) return false;
        switch($all[$id]['type'] ) {
//            case 'headerimage':
//            case 'image':
//            case 'video':
//            case 'google-maps':
//            case 'slideshow':
//                $type = $all[$id]['type'];
//                $media[$type][] = $all[$id];
//                break;
            case 'container':
                $all[$id]['items'] = [];
                foreach ($all[$id]['item_ids'] as $item_id) {
                    $all[$id]['items'][] = $this->sortElements( $item_id, $all, $text, $media, false);
                }
                if( !$first) return $all[$id];
                unset( $all[$id]['item_ids']);
                $text[] = $all[$id];
                break;
            default:
                if( !$first) return $all[$id];
                $text[] = $all[$id];
                break;
        }
        return true;
    }
}