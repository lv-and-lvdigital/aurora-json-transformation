<?php

/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 17.01.2019
 * Time: 10:29
 */

namespace lv\aurorajson\manager;


use Aws\Sns\Message;
use Aws\Sns\MessageValidator;
use GuzzleHttp\Client;
use Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use ZipArchive;


/**
 * Class DownloadManager
 *
 *
 * @package lv\aurorajson\manager
 *
 */
class DownloadManager
{
    /** @var array */
    private $configData;
    /** @var Logger  */
    private $log;
    /** @var string Article  Data from WoodwingAWS-Cloud */
    private $articleJsonData;
    /** @var String Article Meta Data from Woodwing-AWS-Cloud */
    private $metaData;
    /** @var Array Article Notification Data from Woodwing-AWS-Cloud */
    private $notificationData;
    /** @var boolean downloadSuccess */
    private $downloadSuccess = true;

    /**
     * @param array
     * ADownloadManager constructor.
     */
    public function __construct($configData)
    {
        $this->configData = $configData;

        /** @var Logger $log */
        $this->log = new Logger('wac');
        try {
            $this->log->pushHandler(new StreamHandler($configData['log'], Logger::INFO));
        } catch (\Exception $e) {
            echo "can't instantiate logger";
        }
    }

    /**
     *
     */
    public function handleAwsSnsMessage()
    {
        $message = Message::fromRawPostData();

        $validator = new MessageValidator();
        $validator->validate($message);

        $type = $message->offsetGet('Type');

        if ($type === 'SubscriptionConfirmation') {
            $this->processSubscriptionMessage($message);
        } else if ($type === 'Notification') {
            $msg = json_decode($message->offsetGet('Message'));

            if (isset($msg->storyCount)) {
                $this->processBulkPublishAnnounceMessage($msg);
            } else {
                $this->processPublishMessage($msg);
            }
        }
    }
    /**
     * Confirms that the endpoint is valid and can be used.
     * This allows the endpoint to receive further messages.
     */
    private function processSubscriptionMessage(Message $message)
    {
        $httpClient = new Client();
        $httpClient->get($message->offsetGet('SubscribeURL'));
        $this->log->info("Confirmation of valid Endpoint: " . $message->offsetGet('SubscribeURL'));
    }

    /**
     * Processes a bulk publish announce message.
     * Tells the custom channel multiple stories are being published through one action.
     */
    private function processBulkPublishAnnounceMessage($msg)
    {
        Log::info('Bulk publishing ' . $msg->storyCount . ' stories');
    }

    /**
     * Processes a publish action.
     */
    private function processPublishMessage($msg)
    {
        $tmpRealPath = realpath(getcwd() . '/../public/tmp');
        $tmpath = tempnam($tmpRealPath, '/article_');
        $this->log->info("tmpath ist: " . $tmpath);

        try {
            file_put_contents($tmpath, fopen($msg->url, 'r'));

            $zip = new ZipArchive();
            if ($zip->open($tmpath) === TRUE) {
                $storyPath = realpath(getcwd() . '/../public/stories');
                // deepcode ignore ZipSlip: <please specify a reason of ignoring this>
                if ($zip->extractTo($storyPath . '/' . $msg->id)) {
                    $zip->close();
                    $notificationData = file_get_contents('php://input');
                    file_put_contents(
                        $storyPath . '/' . $msg->id . '/notification.json',
                        $notificationData
                    );
                    $this->notificationData = json_decode(file_get_contents($storyPath . '/' . $msg->id . '/notification.json'), true);
                    $articleJsonData = fopen($msg->articleJsonUrl, 'r');
                    file_put_contents(
                        $storyPath . '/' . $msg->id . '/article1.json',
                        $articleJsonData
                    );
                    $this->articleJsonData = file_get_contents($storyPath . '/' . $msg->id . '/article1.json');
                    $metaJsonData = fopen($msg->metadataUrl, 'r');
                    file_put_contents(
                        $storyPath . '/' . $msg->id . '/metadata.json',
                        $metaJsonData
                    );
                    $this->metaData = json_decode(file_get_contents($storyPath . '/' . $msg->id . '/metadata.json'), true);
                } else {
                    $this->log->err("zip file could not be extract: " . $tmpath);
                    return;
                }
            }
        } catch (\Exception $e) {
            $this->downloadSuccess = true;
            $this->log->err("error unzip Data from aws: " . $e->getMessage());
        } finally {
            unlink($tmpath);
        }
    }

    /**
     * @return String
     */
    public function getMetaData()
    {
        return $this->metaData;
    }

    /**
     * @return String
     */
    public function getArticleJsonData()
    {
        return $this->articleJsonData;
    }

    /**
     * @return bool
     */
    public function isDownloadSuccess()
    {
        return $this->downloadSuccess;
    }

    /**
     * @return Array
     */
    public function getNotificationData()
    {
        return $this->notificationData;
    }
}
