<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 31.01.2019
 * Time: 13:13
 */
namespace lv\aurorajson\tools;


class ArrayHelper
{
    /**
     * Get value from array by key
     *
     * @param array $array
     * @param string $key
     * @param mixed $default
     * @return mixed
     * @example
     *  $array = [
     *      'contact' => ['Name' => 'Alex']
     *  ];
     *
     *  echo array_get($array, 'contact.name'); // Alex
     *  echo array_get($array, 'contact.fullname', 'Anonymous'); // Anonymous
     */
    public static function array_get($array, $key, $default = null)
    {
        if (!is_array($array)) {
            return $default;
        }
        if (is_null($key)) {
            return $array;
        }
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }
        if (strpos($key, '.') === false) {
            return $default;
        }
        foreach (explode('.', $key) as $segment) {
            if (is_array($array) && array_key_exists($segment, $array)) {
                $array = $array[$segment];
            } else {
                return $default;
            }
        }
        return $array;
    }
}