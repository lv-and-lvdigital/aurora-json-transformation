<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TextTransformTextShadow {

    public function textTransform($data)
    {
        $blur = ArrayHelper::array_get($data, "blur", "");
        $color = ArrayHelper::array_get($data, "color", "");
        $h = ArrayHelper::array_get($data, "x", "");
        $v = ArrayHelper::array_get($data, "y", "");
        $textShadow = '<span style="text-shadow: ' . $h . 'px ' . $v . 'px ' . $blur . 'px ' . $color.';">%s</span>';
        return $textShadow;
    }
}