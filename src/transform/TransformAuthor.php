<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformAuthor extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $name = parent::transform( ArrayHelper::array_get($container, 'content.name', []));
                $link = parent::transform( ArrayHelper::array_get($container, 'content.link', []));
                $list[$id] = compact( 'type', 'name', 'link');
            }
        }
        return $list;
    }
}