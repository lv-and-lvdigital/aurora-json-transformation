<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:03
 */

namespace lv\aurorajson\transform;


use Exception;
use lv\aurorajson\tools\ArrayHelper;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Noodlehaus\Config;
use ReflectionException;
use ReflectionMethod;

class TransformBase
{
    /** @var array|null */
    private $configData;
    /** @var Logger $log */
    private $log;

    /**
     * TransformerBase constructor.
     */
    public function __construct()
    {
        /* load config of each website from config.json */
        $conf = Config::load('../config/config.json');
        $this->configData = $conf->all();

        /** @var Logger $log */
        $this->log = new Logger('wac');
        try {
            $this->log->pushHandler(new StreamHandler($this->configData['log'], Logger::INFO));
        } catch ( Exception $e) {
            print "Cannot create log file.\n";
        }
    }


    /**
     * @param array $data
     * @return mixed $transformed
     *
     */
    public function transform($data, $doTransformation = true)
    {
        $textTrans = $this->configData['textTrans'];
        $trs = '';
        if (is_array($data)) {
            foreach ($data as $element) {
                $insert = ArrayHelper::array_get($element, 'insert', '');
                if ($doTransformation) {
                    $attributes = ArrayHelper::array_get($element, 'attributes', false);
                    if ($attributes) {
                        foreach ($attributes as $attribute => $attributeValue) {
                            $this->log->info('attribute: ' . var_export($attribute, true));
                            $this->log->info('attributeValue: ' . var_export($attributeValue, true));
                            if (is_string($attribute)) {
                                if (is_array($attributeValue)) {
                                    $textTransHtml = $this->transformData($textTrans, $attribute, $attributeValue);
                                } else {
                                    $textTransHtml = ArrayHelper::array_get($textTrans, $attribute, '');
                                    $this->log->info('textTransHtml: ' . $textTransHtml);
                                }
                                if (!empty($textTransHtml)) {
                                    if (substr_count($textTransHtml, '%s') > 1) {
                                        $insert = sprintf($textTransHtml, $attributeValue, $insert);
                                    } else {
                                        $insert = sprintf($textTransHtml, $insert);
                                    }
                                }
                            }
                        }
                    }
                }
                $trs .= $insert;
            }
        }
        return $trs;
    }

    /**
     * @param $textTrans
     * @param $attribute
     * @param $attributeValue
     * @return String transformed
     */
    private function transformData($textTrans, $attribute, $attributeValue): string
    {
        $transformed = '';
        $this->log->info('AttributeValue is an Array....');
        $checkClass = ArrayHelper::array_get($textTrans, $attribute, '');
        if (class_exists($checkClass)) {
            try {
                $transformer = new $checkClass();
                $reflectionMethod = new ReflectionMethod($checkClass, 'textTransform');
                $transformed = $reflectionMethod->invokeArgs($transformer,
                    array($attributeValue)); // throws exception if parameters are wrong
            } catch (ReflectionException $e) {
                $this->log->error('error executing transformation: class: ' . $checkClass . ' Exception: ' . $e->getMessage());
            }
        }
        return $transformed;
    }

    protected function transformImage(array $element): array
    {

        $imageSrc = ArrayHelper::array_get($element, 'image.id', '');
        $focuspoint = json_encode( ArrayHelper::array_get($element, 'image.focuspoint', false) );
        $cropper = json_encode( ArrayHelper::array_get($element, 'image.cropper', false) );
        $caption = TransformBase::transform(ArrayHelper::array_get($element, "caption", ""));
        $credit = TransformBase::transform(ArrayHelper::array_get($element, "credit", ""));
        $alt = TransformBase::transform( ArrayHelper::array_get($element, 'alternativ', ""));
        $img_title = TransformBase::transform( ArrayHelper::array_get($element, 'title', ""));

        return [$imageSrc, $focuspoint, $cropper, $caption, $credit, $alt, $img_title];

    }
}