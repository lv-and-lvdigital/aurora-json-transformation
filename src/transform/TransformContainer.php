<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;





use lv\aurorajson\tools\ArrayHelper;

class TransformContainer extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
//        return $data;

        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $items = ArrayHelper::array_get($container, 'containers.main', []);
                $ids = [];
                foreach( $items as $item) {
                    $ids[] = $item['id'];
                }
                $info = ['type' => $type, 'item_ids' => $ids];
                $list[$id] = $info;
            }
        }
        return $list;

    }


}