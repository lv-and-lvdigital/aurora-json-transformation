<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformContentReference extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $json_data_array = ArrayHelper::array_get($container, 'content.here.data', []);
                $json_object = json_decode($json_data_array);
                var_dump(parent::transform( ArrayHelper::array_get($container, 'content.here.data', [])));
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $uuid = $json_object->id;
                $headline = $json_object->title;
                $teasertext = $json_object->teasertext;
                $url = $json_object->article_url;
                $list[$id] = compact('type', 'uuid', 'headline', 'teasertext', 'url');
            }
        }
        return $list;
    }
}