<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformEmbed extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $html = ArrayHelper::array_get($container, 'content.html', '');
                $title = parent::transform( ArrayHelper::array_get($container, 'content.title', []));
                $list[$id] = compact( 'type', 'html' , 'title');
            }
        }
        return $list;
    }
}