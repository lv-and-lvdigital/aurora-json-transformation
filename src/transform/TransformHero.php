<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformHero extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach ($data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                list($imageSrc, $focuspoint, $cropper, $caption, $credit, $alt, $img_title) = $this->transformImage(ArrayHelper::array_get($container,
                    'content', []));
                $subtitle = parent::transform(ArrayHelper::array_get($container, 'content.subtitle', []));
                $title = parent::transform(ArrayHelper::array_get($container, 'content.title', []));
                $author = parent::transform(ArrayHelper::array_get($container, 'content.author', []));
                $list[$id] = compact('type', 'imageSrc', 'title', 'subtitle', 'author', 'focuspoint', 'cropper',
                    'caption', 'credit', 'alt', 'img_title');
            }
        }
        return $list;
    }
}