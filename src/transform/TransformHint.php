<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformHint extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $containers = ArrayHelper::array_get($container, 'containers.main', []);
                $crosshead = '';
                $text = '';
                $link = '';
                foreach( $containers as $cont) {
                    switch( $cont['identifier']) {
                        case 'crosshead':
                            if( empty( $crosshead)) {
                                $crosshead = parent::transform( ArrayHelper::array_get($cont, 'content.text', []));
                            }
                            break;
                        case 'body':
                            $text .= '<p>'. parent::transform( ArrayHelper::array_get($cont, 'content.text', [])) . '</p>';
                            break;
                        case 'link':
                            if( empty( $link)) {
                                $linktext = parent::transform( ArrayHelper::array_get($cont, 'content.text', []));
                                $linkurl = parent::transform( ArrayHelper::array_get($cont, 'content.link', []), false);
                                $link = "<a href=\"$linkurl\">$linktext</a>";
                            }
                            break;
                        default:
                            break;
                    }
                }
                $list[$id] = compact( 'type', 'crosshead', 'link', 'text');
            }
        }
        return $list;
    }
}