<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformImage extends TransformBase
{
    public function transform($data, $doTransformation = true): array
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                list( $imageSrc, $focuspoint, $cropper, $caption, $credit, $alt, $img_title) = $this->transformImage( ArrayHelper::array_get($container, 'content', []));
                $hyperlink = ArrayHelper::array_get($container, 'content.hyperlink', '');
//                $caption = parent::transform( ArrayHelper::array_get($container, 'content.caption', []));
//                $alt = parent::transform( ArrayHelper::array_get($container, 'content.alternativ', []));
//                $credit = parent::transform( ArrayHelper::array_get($container, 'content.credit', []));
                $list[$id] = compact('type', 'imageSrc', 'caption', 'credit', 'alt', 'hyperlink',
                    'focuspoint', 'cropper', 'img_title');
            }
        }
        return $list;
    }
}