<?php
/**
 * Created by VSCode.
 * User: Christian Gerdes
 * Date: 20.08.2020
 * Time: 09:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformInterview extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $text = parent::transform( ArrayHelper::array_get($container, 'content.text', []));
                $person = parent::transform( ArrayHelper::array_get($container, 'content.person', []));
                $list[$id] = compact('type', 'text', 'person');
            }
        }
        return $list;
    }
}