<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformLink extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $text = parent::transform( ArrayHelper::array_get($container, 'content.text', []));
                $link_array = ArrayHelper::array_get($container, 'content.link', []);
                $link = ArrayHelper::array_get($link_array[0], 'attributes.href', []);

                // Wenn Link leer dann Plaintext nehmen
                if(empty($link))
                {
                    $link = parent::transform( ArrayHelper::array_get($container, 'content.link', []), false);
                }
                // Wenn Link valide URL dann einfuegen
                if(filter_var($link, FILTER_VALIDATE_URL))
                {
                    $list[$id] = compact( 'type', 'text', 'link');
                }
            }
        }
        return $list;
    }
}