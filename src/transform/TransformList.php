<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformList extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
//        var_dump($data);
//        exit;
//        return $data;
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
//                print_r( $id);
//                exit;
//                print_r( $container);
//                exit;
                $info = ['type' => $type, 'items' => []];
                foreach ($container['containers']['main'] as $element) {
                    $content_text = ArrayHelper::array_get($element, 'content.text', []);
//                $styles = ArrayHelper::array_get($element, 'styles', array() );
                    $listItem = parent::transform( $content_text);
                    $info['items'][] = $listItem;
                }
                $list[$id] = $info;
            }
        }
        return $list;
    }
}