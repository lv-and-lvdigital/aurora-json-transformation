<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformMaps extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $mapTypeId = ArrayHelper::array_get($container, 'content.gmap.options.mapTypeId', '');
                $zoom = ArrayHelper::array_get($container, 'content.gmap.options.zoom', '16');
                $lat = ArrayHelper::array_get($container, 'content.gmap.options.center.lat', '0');
                $lng = ArrayHelper::array_get($container, 'content.gmap.options.center.lng', '0');
                $markers = ArrayHelper::array_get($container, 'content.gmap.markers', []);
                $marker_data = [];
                foreach( $markers as $marker) {
                    $marker_lat = ArrayHelper::array_get($marker, 'position.lat', '0');
                    $marker_lng = ArrayHelper::array_get($marker, 'position.lng', '0');
                    $marker_title = ArrayHelper::array_get($marker, 'title', '0');
                    $marker_icon = ArrayHelper::array_get($marker, 'icon.url', '0');
                    $marker_data[] = ['lat' => $marker_lat, 'lng' => $marker_lng, 'title' => $marker_title, 'icon' => $marker_icon];
                }
                $info = ['type' => $type, 'mapTypeId' => $mapTypeId, 'zoom' => $zoom, 'lat' => $lat, 'lng' => $lng, 'marker' => $marker_data];
                $list[$id] = $info;
            }
        }
        return $list;
    }
}