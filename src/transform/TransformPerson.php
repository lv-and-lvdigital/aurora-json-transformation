<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformPerson extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $title = parent::transform( ArrayHelper::array_get($container, 'content.title', []));
                $givenname = parent::transform( ArrayHelper::array_get($container, 'content.givenname', []));
                $surname = parent::transform( ArrayHelper::array_get($container, 'content.surname', []));
                $description = parent::transform( ArrayHelper::array_get($container, 'content.description', []));
                $position = parent::transform( ArrayHelper::array_get($container, 'content.position', []));
                $contact = parent::transform( ArrayHelper::array_get($container, 'content.contact', []));
                list( $imageSrc, $focuspoint, $cropper, $caption, $credit, $alt, $img_title) = $this->transformImage( ArrayHelper::array_get($container, 'content', []));
                $list[$id] = compact( 'type', 'title', 'givenname', 'surname', 'description', 'contact',
                    'position', 'imageSrc', 'focuspoint', 'cropper', 'caption', 'credit', 'alt', 'img_title');
            }
        }
        return $list;
    }
}