<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformQuote extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $text = parent::transform( ArrayHelper::array_get($container, 'content.text', []));
                $author = parent::transform( ArrayHelper::array_get($container, 'content.author', []));
                $list[$id] = compact('type', 'text', 'author');
            }
        }
        return $list;
    }
}