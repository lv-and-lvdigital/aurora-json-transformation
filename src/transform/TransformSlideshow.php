<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformSlideshow extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $title = parent::transform( ArrayHelper::array_get($container, 'content.title', []));
                $slideshow = [];
                foreach ($container['containers']['slideshow'] as $element) {
                    list( $imageSrc, $focuspoint, $cropper, $caption, $credit, $alt, $img_title) = $this->transformImage( ArrayHelper::array_get($element, 'content', []));
                    $image = compact( 'imageSrc', 'focuspoint', 'cropper', 'caption', 'credit', 'alt', 'img_title');
                    $slideshow[] = $image;
                }
                $list[$id] = compact( 'type', 'slideshow', 'title');
            }
        }
        return $list;
    }
}