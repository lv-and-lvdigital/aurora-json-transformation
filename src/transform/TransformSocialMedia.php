<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformSocialMedia extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $html = ArrayHelper::array_get($container, 'content.html', '');
                $url = ArrayHelper::array_get($container, 'content.media.url', '');
                $provider = ArrayHelper::array_get($container, 'content.media.provider', '');
                $includeMedia = ArrayHelper::array_get($container, 'content.media.properties.include-media', '');
                $list[$id] = compact( 'type', 'html', 'url', 'provider', 'includeMedia');
            }
        }
        return $list;
    }
}