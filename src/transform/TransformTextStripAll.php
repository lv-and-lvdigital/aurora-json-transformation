<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

class TransformTextStripAll extends TransformText
{
    public function transform($data, $doTransformation = true)
    {
        return parent::transform( $data, false);
    }
}