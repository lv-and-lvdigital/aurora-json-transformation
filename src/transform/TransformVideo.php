<?php
/**
 * Created by PhpStorm.
 * User: michael.ponewass
 * Date: 30.01.2019
 * Time: 16:10
 */

namespace lv\aurorajson\transform;

use lv\aurorajson\tools\ArrayHelper;

class TransformVideo extends TransformBase
{
    public function transform($data, $doTransformation = true)
    {
        $list = [];
        if (is_array($data)) {
            foreach( $data as $container) {
                $id = ArrayHelper::array_get($container, 'id', '');
                $type = ArrayHelper::array_get($container, 'identifier', '');
                $url = ArrayHelper::array_get($container, 'content.media.url', '');
                $provider = ArrayHelper::array_get($container, 'content.media.provider', '');
                $html = ArrayHelper::array_get($container, 'content.html', []);
                $list[$id] = compact('type', 'url', 'provider', 'html');
            }
        }
        return $list;
    }
}