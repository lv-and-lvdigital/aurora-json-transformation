<?php

class GutenbergParser {

	static function parseChild(DOMDocument $dom, DOMNode $child) {
		$embraceStart = $embraceEnd = "";
		$extraType = '';
		$tag = $child->nodeName;
		$classes = explode(' ', $child->getAttribute('class'));
		switch ($tag) {
			case 'h1':
			case 'h2':
			case 'h3':
			case 'h4':
			case 'h5':

				if (in_array('intro', $classes)) {

					$type = "wp:subhead";
					$innerHtml = self::DOMinnerHTML($child);
					$html = "<p class=\"wp-block-subhead\">$innerHtml</p>";
				} elseif (in_array('crosshead', $classes)) {

					$type = "wp:heading";
					$innerHtml = self::DOMinnerHTML($child);
					$html = "<h3>$innerHtml</h3>";
				} else {

					$type = 'wp:heading';
					$innerHtml = self::DOMinnerHTML($child);
					$html = "<$tag>$innerHtml</$tag>";
				}

				break;
			case 'p':
				$type = 'wp:paragraph';
				$innerHtml = self::DOMinnerHTML($child);
				$html = "<p>$innerHtml</p>";

				break;
			case 'blockquote':
				$type = 'wp:quote';
				self::getBlockquote($child, $quote, $author);
				$html = "<blockquote class=\"wp-block-quote\"><p>$quote</p><cite>$author</cite></blockquote>";

				break;
			case 'hr':
				$type = "wp:separator";
				$html = "<hr class=\"wp-block-separator\" />";
				break;
				
			case 'ol':
				$type = "wp:list {\"ordered\":true,\"className\":\"ordered-list\"}";
				$innerHtml = self::DOMinnerHTML($child);
				$html = "<ol class=\"ordered-list\">$innerHtml</ol>";
				break;
			case 'ul':
				$type = "wp:list {\"className\":\"unordered-list\"}";
				$innerHtml = self::DOMinnerHTML($child);
				$html = "<ul class=\"unordered-list\">$innerHtml</ol>";
				break;
			case 'figure':
				$type = 'wp:image';
				self::getImage($child, $url, $caption);
				$wp_class_name = 'wp-image-' . get_attachment_id($url);
				$html = "<figure class=\"wp-block-image\"><img src=\"$url\" alt=\"\"/>";
				if (!empty($caption)) {
					$html .= "<figcaption>$caption</figcaption>";
				}
				$html .= "</figure>";

				break;
			case 'div':
				if (in_array('facebook', $classes)) {
					$url = self::getFacebookLink($child);
					$type = 'wp:core-embed/facebook';
					$extraType = "{\"url\":\"$url\",\"type\":\"rich\",\"providerNameSlug\":\"facebook\"}";
					$html = "<figure class=\"wp-block-embed-facebook wp-block-embed is-type-rich is-provider-facebook\">$url</figure>";

				} elseif (in_array('twitter', $classes)) {
					$url = self::getTwitterLink($child);
					$type = 'wp:core-embed/twitter';
					$extraType = "{\"url\":\"$url\",\"type\":\"rich\",\"providerNameSlug\":\"twitter\"}";
					$html = "<figure class=\"wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter\">$url</figure>";

				} elseif (in_array('instagram', $classes)) {
					$url = self::getInstagramLink($child);
					$type = 'wp:core-embed/instagram';
					$extraType = "{\"url\":\"$url\" }";

					$html = "<figure class=\"wp-block-embed-instagram wp-block-embed\">$url</figure>";

				} elseif (in_array('youtube', $classes)) {
					$type = "wp:core-embed/youtube";
					$url = self::getYoutubeLink($child);
					$extraType = "{\"url\":\"$url\",\"type\":\"video\",\"providerNameSlug\":\"youtube\"}";
					$html = "<figure class=\"wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube\">$url</figure>";

				} elseif (in_array('slideshow', $classes)) {
					$images = self::getGalleryImages($child);
					foreach ($images as $image) {
						$wp_attachment_id = get_attachment_id($image['url']);
						$image_ids[] = $wp_attachment_id;
						MyLog("Image IDs: " . print_r($image_ids));
						
					}
					$images_id_string = "";
					for($i = 0; $i < count($image_ids); $i++){
						MyLog("Image-Loop-Count: " . $i);
						if ($i < count($image_ids)-1){
							MyLog("ID: " . "Mit Komma");
							$images_id_string = $images_id_string .$image_ids[$i] .",";
						}
						else {
							MyLog("ID: " . "Ohne Komma");
							$images_id_string = $images_id_string .$image_ids[$i];
						}
						
					}
					$type = "wp:gallery";
					$extraType = "{\"ids\":[$images_id_string], \"linkTo\":\"attachment\"}";
					switch (count($images)) {
						case 1:
							$class = "columns-1";
							break;
						case 2:
							$class = "columns-2";
							break;
						default:
							$class = "columns-3";
							break;
					}
					$site_url = get_site_url();
					$html = "<ul class=\"wp-block-gallery $class is-cropped\">";
					if (!empty($images)) {
						foreach ($images as $image) {
							$wp_attachment_id = get_attachment_id($image['url']);
							$html .= "<li class=\"blocks-gallery-item\"><figure><a href=\"$image[url]\"><img src=\"$image[url]\" alt=\"\" data-id=\"$wp_attachment_id\" data-link=\"$image[url]\" class=\"wp-image-$wp_attachment_id\" /></a>";
							if (!empty($image['caption'])) {
								$html .= "<figcaption>$image[caption]</figcaption>";
							}
							$html .= "</figure></li>";
						}
					}
					$html .= "</ul>";
				} else {
					$type = 'wp:html';
					$html = $dom->saveHTML($child);
				}

				break;
			default:
				$type = 'wp:html';
				$html = $dom->saveHTML($child);

				break;
		}

		$extraType = trim($extraType);
		$extraType = (!empty($extraType)) ? "$extraType " : "";
		$toReturn = "<!-- $type $extraType -->\n";
		$toReturn .= "$embraceStart$html$embraceEnd\n";
		$toReturn .= "<!-- /$type -->\n";
		$toReturn .= "\n";

		return $toReturn;
	}

	static function parseGutenbergBlocks($articleHTML) {

		try {
			$dom = self::HTMLToDOM($articleHTML);

			$className = 'articleContent';

			//find the articleContent
			$article = self::xpath($dom, "//*[contains(@class,'$className')]/*");

			$newHTML = '';
			if (!empty($article)) {
				foreach ($article as $row) {
					$newHTML .= self::parseChild($dom, $row);
				}
			}
			return $newHTML;

		} catch (Exception $e) {
			return $articleHTML;
		}
	}

	static function DOMinnerHTML(DOMNode $element) {
		$innerHTML = "";
		$children = $element->childNodes;

		foreach ($children as $child) {
			$innerHTML .= $element->ownerDocument->saveHTML($child);
		}

		return $innerHTML;
	}

	static function xpath($dom, $query) {
		try {
			$xpath = new DOMXPath($dom);
			return $xpath->evaluate($query);
		} catch (Exception $e) {
			return false;
		}
	}

	static function getGalleryImages($child) {
		$newDom = self::NodeToDocument($child);

		$images = array();
		$element = self::xpath($newDom, "//figure");
		if (!empty($element) && isset($element[0]) && !empty($element[0])) {
			foreach ($element as $image) {
				$url = false;
				$caption = false;
				self::getImage($image, $url, $caption);
				$images[] = array(
					'url'     => $url,
					'caption' => $caption
				);
			}
		}

		return $images;
	}

	static function getFacebookLink(DOMNode $child) {
		$newDom = self::NodeToDocument($child);

		$url = "";
		$element = self::xpath($newDom, "//blockquote");
		if (!empty($element) && isset($element[0]) && !empty($element[0])) {
			$url = $element[0]->getAttribute('cite');
		}
		return self::parseURL($url);
	}

	static function getInstagramLink(DOMNode $child) {
		$newDom = self::NodeToDocument($child);

		$url = "";
		$element = self::xpath($newDom, "//blockquote");
		if (!empty($element) && isset($element[0]) && !empty($element[0])) {
			$url = $element[0]->getAttribute('data-instgrm-permalink');
		}
		return self::parseURL($url);
	}

	static function getTwitterLink(DOMNode $child) {
		$newDom = self::NodeToDocument($child);

		$url = "";
		$element = self::xpath($newDom, "//*[contains(@class,'twitter-timeline')]");
		if (!empty($element) && isset($element[0]) && !empty($element[0])) {
			$url = $element[0]->getAttribute('href');
		}
		return self::parseURL($url);
	}

	static function getYoutubeLink(DOMNode $child) {
		$newDom = self::NodeToDocument($child);
		$url = "";
		$element = self::xpath($newDom, "//iframe");
		if (!empty($element) && isset($element[0]) && !empty($element[0])) {
			$url = $element[0]->getAttribute('src');
		}
		return self::youtubeEmbedToWatch(self::parseURL($url));
	}

	static function parseURL($url) {
		$parts = parse_url($url);
		return "$parts[scheme]://$parts[host]$parts[path]";
	}

	static function youtubeEmbedToWatch($url) {
		return preg_replace("/(http.*com)\/embed\/(.*)/i", "$1/watch?v=$2", $url);
	}

	static function getImage(DOMNode $child, &$url, &$caption) {
		$newDom = self::NodeToDocument($child);

		//extract url
		$url = "";
		$image = self::xpath($newDom, "//*[contains(@doc-image,'image')]");
		if (!empty($image) && isset($image[0]) && !empty($image[0])) {
			$imageHtml = $newDom->saveXML($image[0]);
			preg_match('/background-image:.*url\((.*)\)/im', $imageHtml, $matches);
			if (isset($matches[1])) {
				$url = trim(html_entity_decode($matches[1]), "\"'");
			}
		}

		//extract caption
		$caption = "";
		$captionContainer = self::xpath($newDom, "//*[contains(@doc-editable,'caption')]");
		if (!empty($captionContainer) && isset($captionContainer[0]) && !empty($captionContainer[0])) {
			$caption = self::DOMinnerHTML($captionContainer[0]);
		}
	}

	static function getBlockquote(DOMNode $child, &$quote, &$author) {
		$newDom = self::NodeToDocument($child);

		$quote = self::xpath($newDom, "//*[contains(@class,'quote')]");
		$author = self::xpath($newDom, "//*[contains(@doc-editable,'author')]");

		$quote = (!empty($quote) && isset($quote[0]) && !empty($quote[0])) ? self::DOMinnerHTML($quote[0]) : "";
		$author = (!empty($author) && isset($author[0]) && !empty($author[0])) ? self::DOMinnerHTML($author[0]) : "";
	}

	static function HTMLToDOM($html) {
		$dom = new DOMDocument;
		libxml_use_internal_errors(true);
		$dom->loadHTML("<?xml encoding=\"utf-8\" ?>$html");
		libxml_clear_errors();
		return $dom;
	}

	static function NodeToDocument($child) {
		$newDom = new DOMDocument;
		libxml_use_internal_errors(true);
		$newDom->appendChild($newDom->importNode($child, true));
		libxml_clear_errors();
		return $newDom;
	}

}

